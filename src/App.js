import Axios from "./components/axios";


function App() {
  return (
    <div className="App">
      <Axios/>
    </div>
  );
}

export default App;
