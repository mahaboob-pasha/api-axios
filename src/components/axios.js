import React ,{useState , useEffect} from 'react';
import axios from "axios";
import styles from "./axios.css";

const Axios = () => {

const [comments,setComments]=useState([]);

useEffect (()=>{
    axios.get('https://jsonplaceholder.typicode.com/comments')
    .then(res=>{
        console.log(res)
        setComments(res.data)
    })
    .catch (err=>console.log(err))
},[])


    return ( 

    <div className="wrapper">
        {
            comments.map(comment=>(
                <div className="inner" key={comments.id}>
                 <h1>{comment.name}</h1>
                 <h2>{comment.email}</h2>
                 <p>{comment.body}</p>
                 </div>
            )
            )
            
        }
        
    </div>
     );
}
 
export default Axios;